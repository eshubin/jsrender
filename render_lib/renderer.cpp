#include "renderer.h"


#include <QtWebKit/QWebFrame>

Renderer::Renderer(const QUrl& url, const QString& script, QObject *parent) :
    QObject(parent), script_(script)
{
    connect(&page_, SIGNAL(loadFinished(bool)),
             this, SLOT(render(bool)));
    page_.mainFrame()->load(url);
}

void Renderer::render(bool success)
{
    QString result;
    if (success)
    {
        QWebFrame *frame = page_.mainFrame();
        if (!script_.isEmpty())
        {
            frame->evaluateJavaScript(script_);
        }
        result = frame->toHtml();
    }
    emit finished(result);
}
