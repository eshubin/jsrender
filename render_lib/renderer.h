#ifndef RENDERER_H
#define RENDERER_H

#include <QtWebKit/QWebPage>

//class QUrl;

class Renderer : public QObject
{
    Q_OBJECT

public:
    Renderer(const QUrl& url, const QString& script = QString(), QObject *parent = NULL);

signals:
    void finished(const QString&);

private slots:
     void render(bool success);

private:
    QWebPage page_;
    QString script_;
};

#endif // RENDERER_H
