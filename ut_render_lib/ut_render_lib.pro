# -------------------------------------------------
# Project created by QtCreator 2012-07-07T00:43:51
# -------------------------------------------------
QT += webkit \
    testlib
TARGET = render_lib_test
CONFIG += console
CONFIG -= app_bundle
TEMPLATE = app
SOURCES += render_lib_test.cpp \
    render.cpp \
    contentsaver.cpp
DEFINES += SRCDIR=\\\"$$PWD/\\\"
HEADERS += render.h \
    contentsaver.h
INCLUDEPATH += ..
LIBS += -L../render_lib -lrender_lib
