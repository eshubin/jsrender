#include "contentsaver.h"

ContentSaver::ContentSaver(QObject *parent) :
    QObject(parent)
{
}

QString ContentSaver::get() const
{
    return text_;
}

void ContentSaver::save(const QString &text)
{
    text_ = text;
}
