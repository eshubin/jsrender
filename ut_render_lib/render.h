#ifndef RENDER_H
#define RENDER_H

#include <QtCore/QString>

class QUrl;

QString render(const QUrl& url, const QString& script = QString());

#endif // RENDER_H
