#include "render.h"

#include <QtCore/QEventLoop>
#include <render_lib/renderer.h>

#include "contentsaver.h"

QString render(const QUrl& url, const QString& script)
{
    Renderer ren(url, script);
    ContentSaver saver;
    QEventLoop loop;
    QObject::connect(
                    &ren, SIGNAL(finished(const QString&)),
                    &saver, SLOT(save(const QString&))
                    );
    QObject::connect(&ren, SIGNAL(finished(const QString&)),
                     &loop, SLOT(quit()));



    loop.exec();
    return saver.get();
}

