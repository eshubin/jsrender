#include <QtCore/QString>
#include <QtTest/QtTest>
#include <QtCore/QCoreApplication>

#include "render.h"

class RenderLibTest : public QObject
{
    Q_OBJECT

public:
    RenderLibTest();

private Q_SLOTS:
    void testSimpleText();
    void testNonExistentUrl();
    void testEmpty();
    void testSimpleJS();
    void testButtonJS();
};

RenderLibTest::RenderLibTest()
{
}

void RenderLibTest::testButtonJS()
{
    QString expected = QString("\
<html><head></head><body>\n\
<div id=\"demo\">bbb</div>\n\
<script type=\"text/javascript\">\n\
function changeContent()\n\
{\n\
var text = document.getElementById(\"demo\").innerHTML;\n\
var new_text = \"\";\n\
for (i in text)\n\
{\n\
    var c = text.charCodeAt(i);\n\
    new_text += String.fromCharCode(c+1);\n\
}\n\
document.getElementById(\"demo\").innerHTML=new_text;\n\
}\n\
</script>\n\
<button id=\"button\" type=\"button\" onclick=\"changeContent()\">Click it</button>\n\
\n\
\n\
</body></html>");
    QCOMPARE(
             render(
                    QUrl(QDir::currentPath() + "/test_data/button_js.html"),
                    "document.getElementById(\"button\").click()"
                    ),
             expected
             );
}


void RenderLibTest::testSimpleJS()
{
    QString expected = QString("\
<html><head></head><body>\n\
<div id=\"demo\">bbb</div>\n\
<script type=\"text/javascript\">\n\
var text = document.getElementById(\"demo\").innerHTML;\n\
var new_text = \"\";\n\
for (i in text)\n\
{\n\
    var c = text.charCodeAt(i);\n\
    new_text += String.fromCharCode(c+1);\n\
}\n\
document.getElementById(\"demo\").innerHTML=new_text;\n\
</script>\n\
\n\
\n\
</body></html>");
    QCOMPARE(
             render(QUrl(QDir::currentPath() + "/test_data/simple_js.html")),
             expected
             );
}

void RenderLibTest::testSimpleText()
{
    QCOMPARE(
             render(QUrl(QDir::currentPath() + "/test_data/simple_text.html")),
             QString("<html><head></head><body>aaa</body></html>")
             );
}

void RenderLibTest::testNonExistentUrl()
{
    QVERIFY(
            render(QUrl(QDir::currentPath() + "/test_data/non_existent.html")).isNull()
            );
}

void RenderLibTest::testEmpty()
{
    QString res = render(QUrl(QDir::currentPath() + "/test_data/empty.html"));
    QVERIFY(
            !res.isNull()
            );
    QCOMPARE(
            res, QString("<html><head></head><body></body></html>")
            );
}

QTEST_MAIN(RenderLibTest);

#include "render_lib_test.moc"
