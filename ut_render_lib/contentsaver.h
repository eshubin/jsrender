#ifndef CONTENTSAVER_H
#define CONTENTSAVER_H

#include <QObject>

class ContentSaver : public QObject
{
Q_OBJECT
public:
    explicit ContentSaver(QObject *parent = 0);
    QString get() const;

signals:

public slots:
    void save(const QString& text);

private:
    QString text_;

};

#endif // CONTENTSAVER_H
